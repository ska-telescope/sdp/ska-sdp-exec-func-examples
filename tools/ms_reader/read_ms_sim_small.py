import optparse, os, pickle
import ms_operations

'''
- This code reads .ms files. It has been tested with "sim_small.ms".
- The code has not yet been adapted fully to access 2x2 visibility matrices; it currently works best with single visibility values instead.
- Command line options including "-n", "-p", "--no_output", and "--no_plot" are not yet fully operational.
- To run this code, save the this file along with "ms_operations.py" and the .ms file in one directory and run from the command line (e.g., $ python3 read_ms_sim_small.py sim_small.ms).
- The code will produce .db files including the data.
'''


def main():
    # Terminal configuration
    usage = '%prog [options] ms_file'
    parser = optparse.OptionParser(
        usage=usage,
        version='1.0',
    )

    parser.add_option('-n', '--no_output', help='do not generate any output files or plots. Access functionality from the terminal instead.', default=False)
    parser.add_option('-p', '--no_plot', help='do not generate any plots.', default=False)

    (options, args) = parser.parse_args()

    # Ensuring .ms file has been called
    if len(args) != 1:
        raise ImportError('No .ms file detected. Run this script with a .ms file.')
    
    # Initiating an instance of the ReadMS with the provided .ms file
    msFile = args[0].rstrip()
    msFileName = os.path.basename(msFile)
    
    measurementSet = ms_operations.ReadMS(msFileName)

    # Making a ./Results directory to save outputs (if this does not exist)
    if not os.path.exists(os.path.join(os.getcwd(), 'Results')):
        os.mkdir(os.path.join(os.getcwd(), 'Results'))

    # Saving visibilities, raw uvw coordinates as .db files
    for column, output in [ ['DATA', 'vis.db'], ['UVW', 'uvw_raw.db'] ]:
        param = measurementSet.GetMainTableData(column)
        outputFile = open(os.path.join('Results', output), 'wb')
        pickle.dump(param, outputFile)
        outputFile.close()

    # Saving frequency channels as .db file
    frequencyChannels = measurementSet.UpdateFrequencyChannels()
    outputFile = open(os.path.join('Results', 'freq_chan.db'), 'wb')
    pickle.dump(frequencyChannels, outputFile)
    outputFile.close()

    # Saving polarizations as .db file
    polarizationData = measurementSet.GetPolarizationData()
    outputFile = open(os.path.join('Results', 'polar.db'), 'wb')
    pickle.dump(polarizationData, outputFile)
    outputFile.close()

    # saving u, v, and w coordinates (in wave numbers) as .db files
    measurementSet.ConvertUVWToWavelengths()
    for coordinate, output in [ [measurementSet.u, 'u.db'], [measurementSet.v, 'v.db'], [measurementSet.w, 'w.db'] ]:
        outputFile = open(os.path.join('Results', output), 'wb')
        pickle.dump(coordinate, outputFile)
        outputFile.close()

    # Saving a uv plot of a 2D version of the sampling function, i.e., S = f(u,v)
    measurementSet.PlotSamplingFunction(freqChannel='all')
    
    return None
    

if __name__ == '__main__':
    main()