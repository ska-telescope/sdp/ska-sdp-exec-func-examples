

Refer to the license associated with this repository.

This programme allows you to compare two .fits sky images incorporating some antenna pointing error. A reference image mst be provided (reference.fits) in the data folder. A text file (image_params.txt) must also be provided with some information about the images as provided.

The directory of the image incorporating the poiting error must be provided when running the programme from the command line, e.g.,:

$ python3 pointing_error path/to/fits_image.fits

Dependencies:
- Astropy
- Matplotlib
- Numpy

This programme was developed and tested on Python 3.9.

Refer to the commad line help for runtime options.
