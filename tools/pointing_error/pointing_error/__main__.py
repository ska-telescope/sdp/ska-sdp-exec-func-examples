import argparse
import os

from operations import processor
from utils import fits


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description="Quantify pointing error",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    
    parser.add_argument(
        "image",
        type=str,
        help="directory of image with pointing error (.fits)",
    )
    parser.add_argument(
        "--name",
        type=str,
        default=None,
        help="name for output files",
    )

    return parser.parse_args()


def log_errors(args: argparse.Namespace) -> None:
    if not os.path.exists(args.image):
        raise FileNotFoundError("no such .fits file for the image")
    
    if not os.path.exists(os.path.join(os.getcwd(), "data", "reference.fits")):
        raise FileNotFoundError("expected reference image: ./pointing_error_image/data/reference.fits")

    if not os.path.exists(os.path.join(os.getcwd(), "data", "image_params.txt")):
        raise FileNotFoundError("expected input image parameters: ./pointing_error_image/data/image_params.txt")
    
    return


def read_file(directory: str) -> dict:
    with open(directory, "r") as file:
        lines = file.readlines()
    
    params = {}
    for line in lines:
        if line.strip()[0:3].lower() == "fov":
            params["fov"] = line.strip()[3:].replace(" ", "")
    
    if "fov" in params.keys():
        if params["fov"].replace(".", "").isdigit():
            params["fov"] = float(params["fov"])
        else:
            raise ValueError("bad FoV definition")
    else:
        raise ValueError("FoV not defined in image_params.txt")

    if params["fov"] > 180. or params["fov"] <= 0.:
        raise ValueError("bad FoV value")
    
    return params


def main() -> None:
    args = parse_args()
    log_errors(args)

    image = fits.read_image(os.path.abspath(args.image))
    image_ref = fits.read_image(os.path.join("data", "reference.fits"))
    params = read_file(os.path.join("data", "image_params.txt"))
    
    processor.process_data(
        image,
        image_ref,
        params["fov"],
        name=args.name,
    )
    
    return


if __name__ == "__main__":
    main()
