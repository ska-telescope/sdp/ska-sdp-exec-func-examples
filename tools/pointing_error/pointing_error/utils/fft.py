import numpy as np


def center_axis(input: np.ndarray) -> np.ndarray:
    index_amended = np.empty(0, dtype=int)

    # input array length is even
    if input.shape[0] % 2 == 0:
        index_amended = np.append(
            index_amended,
            np.arange(
                int(0.5*input.shape[0]),
                int(input.shape[0]),
            ),
        )

        index_amended = np.append(
            index_amended,
            np.arange(
                int(0.5*input.shape[0]),
            ),
        )

    # input array length is odd
    else:
        index_amended = np.append(
            index_amended,
            np.arange(
                int(0.5*(input.shape[0]+1)),
                int(input.shape[0]),
            ),
        )

        index_amended = np.append(
            index_amended,
            np.arange(
                int(0.5*(input.shape[0]+1)),
            ),
        )
    
    return input[index_amended]

def center_plot(input: np.ndarray) -> np.ndarray:
    #.....AXIS_0.....
    index_amended_axis0 = np.empty(0, dtype=int)

    # even length along axis 0
    if input.shape[0] % 2 == 0:
        index_amended_axis0 = np.append(
            index_amended_axis0,
            np.arange(
                int(0.5*input.shape[0]),
                int(input.shape[0]),
            ),
        )

        index_amended_axis0 = np.append(
            index_amended_axis0,
            np.arange(
                int(0.5*input.shape[0]),
            ),
        )

    # odd length along axis 0
    else:
        index_amended_axis0 = np.append(
            index_amended_axis0,
            np.arange(
                int(0.5*(input.shape[0]+1)),
                int(input.shape[0]),
            ),
        )

        index_amended_axis0 = np.append(
            index_amended_axis0,
            np.arange(
                int(0.5*(input.shape[0]+1)),
            )
        )

    #.....AXIS_1.....
    index_amended_axis1 = np.empty(0, dtype=int)

    # even length along axis 1
    if input.shape[1] % 2 == 0:
        index_amended_axis1 = np.append(
            index_amended_axis1,
            np.arange(
                int(0.5*input.shape[1]),
                int(input.shape[1]),
            ),
        )

        index_amended_axis1 = np.append(
            index_amended_axis1,
            np.arange(
                int(0.5*input.shape[1])
            ),
        )

    # odd length along axis 1
    else:
        index_amended_axis1 = np.append(
            index_amended_axis1,
            np.arange(
                int(0.5*(input.shape[1]+1)),
                int(input.shape[1]),
            ),
        )

        index_amended_axis1 = np.append(
            index_amended_axis1,
            np.arange(
                int(0.5*(input.shape[1]+1)),
            ),
        )

    output = input[index_amended_axis0]
    for i in range(input.shape[0]):
        row = output[i]
        output[i] = row[index_amended_axis1]

    return output
