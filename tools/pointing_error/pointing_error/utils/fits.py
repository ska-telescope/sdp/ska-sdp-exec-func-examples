import numpy as np

from astropy.io import fits


def read_image(input: str) -> np.ndarray:
    '''
    Assumes standard WSClean image data configuration.
    '''
    image = fits.getdata(input, ext=0)

    while len(image.shape) > 2:
        image = image[0]

    return np.array(image)
