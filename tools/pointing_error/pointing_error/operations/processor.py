from datetime import datetime
import os
from typing import Union

import matplotlib.cm as cm
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import numpy as np

from utils import fft


def create_output_directory(name: str="noname") -> None:
    if not os.path.exists("pointing_outputs"):
        os.mkdir("pointing_outputs")
    
    now = datetime.now()
    with open(os.path.join("pointing_outputs", f"{name}_outputs.txt"), "w") as file:
        file.write("#\n")
        file.write("#\n")
        file.write(f"# Outputs for ( {name} )\n")
        file.write(f"# {now.strftime('This file was generated on %d/%m/%y at %H:%M:%S local time')}\n")
        file.write("#\n")
        file.write("#\n")
        file.write("\n")
        file.write("\n")
    
    return


def plot(
    input: np.ndarray,
    extent: list[float],
    *,
    cmap: Union[str, colors.Colormap]=cm.hot,
    name: str="noname",
) -> None:
    # Fourier visibilities data
    if np.iscomplexobj(input):
        for data, label in [ [np.log(np.abs(input)), "magnitude_log"], [np.angle(input), "phase"] ]:
            plt.figure()
            plt.imshow(data, extent=extent, cmap=cmap)
            plt.xlabel("u")
            plt.ylabel("v")
            plt.title(f"error_visibilities_{label}")
            plt.colorbar()
            plt.savefig(os.path.join("pointing_outputs", f"{name}_fourier_{label}.png"))
            plt.close()

    # Image flux data
    else:
        plt.figure()
        plt.imshow(input, extent=extent, cmap=cmap)
        plt.xlabel("plane angle from phase centre")
        plt.ylabel("plane angle from phase centre")
        plt.title("error_flux")
        plt.colorbar()
        plt.savefig(os.path.join("pointing_outputs", f"{name}_image.png"))
        plt.close()
    
    return


def quantities(
    input: np.ndarray,
    *,
    name: str="noname",
) -> None:    
    if np.iscomplexobj(input):
        rms = {}
        for data, label in [ [np.abs(input), "magnitude"], [np.angle(input), "phase"] ]:
            rms[label] = ( np.sum(data.flatten()**2.) / data.shape[0] )**0.5

    else:
        rms = ( np.sum(input.flatten()**2.) / input.shape[0] )**0.5

    with open(os.path.join("pointing_outputs", f"{name}_outputs.txt"), "a") as file:
        if np.iscomplexobj(input):
            for data, label in [ [np.abs(input), "magnitude"], [np.angle(input), "phase"] ]:
                file.write(f"Values in fourier space ({label})\n")
                file.write(f"MIN    : {np.min(data)}\n")
                file.write(f"MAX    : {np.max(data)}\n")
                file.write(f"MEDIAN : {np.median(data)}\n")
                file.write(f"RMS    : {rms[label]}\n")
                file.write("\n")
                file.write("\n")
        else:
            file.write("Values in image space\n")
            file.write(f"MIN    : {np.min(input)}\n")
            file.write(f"MAX    : {np.max(input)}\n")
            file.write(f"MEDIAN : {np.median(input)}\n")
            file.write(f"RMS    : {rms}\n")
            file.write("\n")
            file.write("\n")
    
    return


def process_data(
    image: np.ndarray,
    image_ref: np.ndarray,
    fov: float,
    *,
    name: str="noname",
) -> None:
    image_diff = image - image_ref
    vis_diff = np.fft.fft2(image_diff)
    vis_diff_centred = fft.center_plot(vis_diff)
    
    fourier_axis = np.fft.fftfreq(
        n=image.shape[0],
        d=2.*np.cos((90.-(fov/2.))*np.pi/180.)/image.shape[0],
    )
    fourier_axis_centred = fft.center_axis(fourier_axis)

    image_extent = [
        90.-fov/2.,
        90.+fov/2.,
        90.-fov/2.,
        90.+fov/2.,
    ]
    vis_extent = [
        fourier_axis_centred[0],
        fourier_axis_centred[-1],
        fourier_axis_centred[0],
        fourier_axis_centred[-1],
    ]

    create_output_directory(name=name)

    for data, extent in [ [image_diff, image_extent], [vis_diff_centred, vis_extent] ]:
        plot(data, extent=extent, name=name)
        quantities(data, name=name)
    
    return
